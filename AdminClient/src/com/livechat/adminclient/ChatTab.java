package com.livechat.adminclient;

import com.livechat.adminclient.opcodes.UserOpcode;
import com.livechat.shared.Packet;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ChatTab {
    private JPanel mainPanel;
    private JTextArea chat;
    private JButton closeChat;
    private JTextField message;

    private final int id;
    private final AdminClient client;

    public ChatTab(final int id, final AdminClient client) {
        this.id = id;
        this.client = client;

        message.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendMessage();
            }
        });
        closeChat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeTab();
            }
        });
        displayStatusMessage("Connected with guest.");
    }

    protected void sendMessage() {
        if (message.getText().length() > 0) {
            client.send(new Packet(UserOpcode.SUPPORT_MESSAGE, id, message.getText()));

            chat.append("[Support]: " + message.getText() + "\n");
            chat.setCaretPosition(chat.getDocument().getLength());
            message.setText("");
        }
    }

    protected void closeTab() {
        client.send(new Packet(UserOpcode.SUPPORT_CLOSE, id));
        mainPanel.getParent().remove(mainPanel);
    }

    public void displayMessage(String message) {
        chat.append("[Guest #"+id+"]: " + message + "\n");
        chat.setCaretPosition(chat.getDocument().getLength());
    }

    public void displayStatusMessage(String message) {
        chat.append(message + "\n");
        chat.setCaretPosition(chat.getDocument().getLength());
    }

    public void closedByGuest() {
        displayStatusMessage("The Guest has closed the conversation.");
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}