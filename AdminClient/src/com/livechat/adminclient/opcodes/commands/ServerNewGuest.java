package com.livechat.adminclient.opcodes.commands;

import com.livechat.adminclient.AdminClient;
import com.livechat.shared.Packet;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ServerNewGuest implements OpcodeCommand {
    @Override
    public void handle(AdminClient client, Packet packet) {
        client.increaseGuestCount();
        client.getMainForm().updateClientsCount();
    }
}
