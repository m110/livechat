package com.livechat.adminclient.opcodes.commands;

import com.livechat.adminclient.AdminClient;
import com.livechat.shared.Packet;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ServerMessage implements OpcodeCommand {
    @Override
    public void handle(AdminClient client, Packet packet) {
        int id = packet.getSenderID();
        String message = packet.getMessage();
        client.getMainForm().displayMessage(id, message);
    }
}
