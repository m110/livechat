package com.livechat.adminclient.opcodes.commands;

import com.livechat.adminclient.AdminClient;
import com.livechat.shared.Packet;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ServerConnect implements OpcodeCommand {
    @Override
    public void handle(AdminClient client, Packet packet) {
        int id = packet.getSenderID();
        client.getMainForm().createChatTab(id);
    }
}
