package com.livechat.adminclient.opcodes;

import com.livechat.adminclient.AdminClient;
import com.livechat.adminclient.opcodes.commands.*;
import com.livechat.shared.Opcode;
import com.livechat.shared.Packet;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum ServerOpcode implements Opcode {
    CONNECT(1, new ServerConnect()),
    MESSAGE(2, new ServerMessage()),
    CLOSE(3, new ServerClose()),
    NEW_GUEST(4, new ServerNewGuest()),
    GUESTS_AMOUNT(5, new ServerGuestsAmount());

    private final int id;
    private final OpcodeCommand opcodeCommand;

    ServerOpcode(int id, OpcodeCommand opcodeCommand) {
        this.id = id;
        this.opcodeCommand = opcodeCommand;
    }

    public void handle(AdminClient client, Packet packet) {
        opcodeCommand.handle(client, packet);
    }

    @Override
    public int getID() {
        return id;
    }

    public static ServerOpcode getByID(int id) {
        for (ServerOpcode opcode : ServerOpcode.values()) {
            if (id == opcode.getID()) {
                return opcode;
            }
        }
        throw new IllegalArgumentException("Such ServerOpcode ID does not exist: " + id);
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
