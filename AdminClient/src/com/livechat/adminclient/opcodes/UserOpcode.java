package com.livechat.adminclient.opcodes;

import com.livechat.shared.Opcode;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum UserOpcode implements Opcode {
    SUPPORT_CONNECT(3),
    SUPPORT_MESSAGE(4),
    SUPPORT_CLOSE(5);

    private final int id;

    UserOpcode(int id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
