package com.livechat.adminclient.core;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum ConnectionStatus {
    CONNECTING, CONNECTED
}
