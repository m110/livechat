package com.livechat.adminclient.core;

import com.livechat.adminclient.AdminClient;
import com.livechat.adminclient.opcodes.ServerOpcode;
import com.livechat.adminclient.opcodes.UserOpcode;
import com.livechat.shared.Packet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Connection implements Runnable {
    private Socket socket;
    private final AdminClient client;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    private boolean running = false;

    public Connection(AdminClient client) {
        this.client = client;
    }

    private boolean connect() {
        try {
            socket = new Socket("localhost", 6688);
            out = new ObjectOutputStream(socket.getOutputStream());

            client.setConnectionStatus(ConnectionStatus.CONNECTED);
            send(new Packet(UserOpcode.SUPPORT_CONNECT, "support"));
            running = true;
            return true;
        } catch (UnknownHostException e) {
            System.out.println("Unknown host!");
            e.printStackTrace();
            client.setConnectionStatus(ConnectionStatus.CONNECTING);
            return false;
        } catch (IOException e) {
            System.out.println("Connection error!");
            client.setConnectionStatus(ConnectionStatus.CONNECTING);
            return false;
        }
    }

    public void run() {
        while (!connect()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("Thread sleep error!");
                e.printStackTrace();
            }
        }

        try {
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (running) {
            try {
                try {
                    Packet packet = (Packet) in.readObject();
                    ServerOpcode.getByID(packet.getOpcodeID()).handle(client, packet);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }

                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("Thread sleep error!");
                e.printStackTrace();
            }
        }
    }

    public void send(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
        } catch (IOException e) {
            System.out.println("Packet sending error!");
            e.printStackTrace();
        }
    }
}
