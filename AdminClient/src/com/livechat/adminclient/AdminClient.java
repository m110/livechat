package com.livechat.adminclient;

import com.livechat.adminclient.core.Connection;
import com.livechat.adminclient.core.ConnectionStatus;
import com.livechat.shared.Packet;

import javax.swing.*;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class AdminClient {

    private final MainForm mainForm;
    private final Connection connection;

    private int guestsCount = 0;

    public AdminClient() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e){
            System.out.println("Could not set Look and Feel!");
            e.printStackTrace();
        }

        mainForm = new MainForm(this);
        connection = new Connection(this);

        JFrame frame = new JFrame("Admin Client");
        frame.setContentPane(mainForm.getMainPanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocation(200, 200);
        frame.pack();
        frame.setVisible(true);

        new Thread(connection).start();
    }

    public void setConnectionStatus(ConnectionStatus status) {
        mainForm.setConnectionStatus(status);
    }

    public void send(Packet packet) {
        connection.send(packet);
    }

    public void increaseGuestCount() {
        guestsCount++;
    }

    public void setGuestsCount(int guestsCount) {
        this.guestsCount = guestsCount;
    }

    public int getGuestsCount() {
        return guestsCount;
    }

    public MainForm getMainForm() {
        return mainForm;
    }

    public static void main(String[] args) {
        new AdminClient();
    }
}
