package com.livechat.adminclient;

import com.livechat.adminclient.core.ConnectionStatus;
import com.livechat.adminclient.opcodes.UserOpcode;
import com.livechat.shared.Packet;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class MainForm {
    private JPanel mainPanel;
    private JPanel sidePanel;
    private JPanel queuePanel;
    private JTabbedPane chatPanel;
    private JLabel connectionStatus;
    private JLabel queueCount;
    private JButton connect;
    private JButton exitButton;

    private final AdminClient client;
    private final HashMap<Integer, ChatTab> chatTabs;

    public MainForm(final AdminClient client) {
        chatTabs = new HashMap<>();
        this.client = client;
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        connect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               client.send(new Packet(UserOpcode.SUPPORT_CONNECT));
            }
        });
    }

    public void createChatTab(int id) {
        ChatTab chat = new ChatTab(id, client);
        JPanel newPanel = chat.getMainPanel();
        chatPanel.addTab("Guest #"+id, newPanel);
        chatPanel.setSelectedComponent(newPanel);
        chatTabs.put(id, chat);
    }

    public void displayMessage(int id, String message) {
        if (chatTabs.containsKey(id)) {
            chatTabs.get(id).displayMessage(message);
        }
    }

    public void updateClientsCount() {
        int count = client.getGuestsCount();
        queueCount.setText(Integer.toString(count));

        if (count > 0) {
            connect.setEnabled(true);
        } else {
            connect.setEnabled(false);
        }
    }

    public void setConnectionStatus(ConnectionStatus status) {
        switch (status) {
        case CONNECTED:
            connectionStatus.setForeground(new Color(51, 153, 0));
            connectionStatus.setText("Connected!");
            break;
        case CONNECTING:
            connectionStatus.setForeground(new Color(0, 0, 0));
            connectionStatus.setText("Connecting...");
            break;
        }
    }

    public void chatClosed(int id) {
        if (chatTabs.containsKey(id)) {
            chatTabs.get(id).closedByGuest();
        }
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}

