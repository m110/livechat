package com.livechat.shared;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public interface Opcode {
    public int getID();
}
