package com.livechat.shared;

import java.io.Serializable;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Packet implements Serializable {
    private int opcodeID;
    private int senderID;
    private String message;

    public Packet(Opcode opcode, int senderID, String message) {
        this.opcodeID = opcode.getID();
        this.senderID = senderID;
        this.message = message;
    }

    public Packet(Opcode opcode, int senderID) {
        this(opcode, senderID, "");
    }

    public Packet(Opcode opcode, String message) {
        this(opcode, 0, message);
    }

    public Packet(Opcode opcode) {
        this(opcode, 0, "");
    }

    public int getOpcodeID() {
        return opcodeID;
    }

    public int getSenderID() {
        return senderID;
    }

    public String getMessage() {
        return message;
    }
}
