package com.livechat.testclient;

import javax.swing.*;

public class TestClient {

    private final Connection connection;
    private final MainForm mainForm;

    public TestClient() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e){
            System.out.println("Could not set Look and Feel!");
            e.printStackTrace();
        }

        mainForm = new MainForm(this);
        connection = new Connection(this);

        JFrame frame = new JFrame("Test Client");
        frame.setContentPane(mainForm.getMainPanel());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocation(200, 200);
        frame.pack();
        frame.setVisible(true);


        new Thread(connection).start();
    }

    public Connection getConnection() {
        return connection;
    }

    public MainForm getMainForm() {
        return mainForm;
    }

    public static void main(String[] args) {
        new TestClient();
    }
}
