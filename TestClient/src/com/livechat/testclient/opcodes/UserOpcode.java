package com.livechat.testclient.opcodes;

import com.livechat.shared.Opcode;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum UserOpcode implements Opcode {
    GUEST_MESSAGE(1),
    GUEST_CLOSE(2);

    private final int id;

    UserOpcode(int id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
