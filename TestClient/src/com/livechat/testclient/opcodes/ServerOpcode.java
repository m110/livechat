package com.livechat.testclient.opcodes;

import com.livechat.shared.Opcode;
import com.livechat.shared.Packet;
import com.livechat.testclient.TestClient;
import com.livechat.testclient.opcodes.commands.OpcodeCommand;
import com.livechat.testclient.opcodes.commands.ServerClose;
import com.livechat.testclient.opcodes.commands.ServerConnect;
import com.livechat.testclient.opcodes.commands.ServerMessage;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum ServerOpcode implements Opcode {
    CONNECT(1, new ServerConnect()),
    MESSAGE(2, new ServerMessage()),
    CLOSE(3, new ServerClose());

    private final int id;
    private final OpcodeCommand opcodeCommand;

    ServerOpcode(int id, OpcodeCommand opcodeCommand) {
        this.id = id;
        this.opcodeCommand = opcodeCommand;
    }

    public void handle(TestClient client, Packet packet) {
        opcodeCommand.handle(client, packet);
    }

    @Override
    public int getID() {
        return id;
    }

    public static ServerOpcode getByID(int id) {
        for (ServerOpcode opcode : ServerOpcode.values()) {
            if (id == opcode.getID()) {
                return opcode;
            }
        }
        throw new IllegalArgumentException("Such ServerOpcode ID does not exist: " + id);
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
