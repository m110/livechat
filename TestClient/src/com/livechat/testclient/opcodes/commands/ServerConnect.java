package com.livechat.testclient.opcodes.commands;

import com.livechat.shared.Packet;
import com.livechat.testclient.TestClient;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class ServerConnect implements OpcodeCommand {
    @Override
    public void handle(TestClient client, Packet packet) {
        client.getMainForm().displayMessage("-- Connected with support");
    }
}
