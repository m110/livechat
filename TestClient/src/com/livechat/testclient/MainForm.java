package com.livechat.testclient;

import com.livechat.shared.Packet;
import com.livechat.testclient.opcodes.UserOpcode;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainForm {

    private final TestClient client;

    private JTextArea chat;
    private JPanel mainPanel;
    private JTextField message;

    public MainForm(final TestClient client) {
        this.client = client;

        message.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (message.getText().length() > 0) {
                    client.getConnection().send(new Packet(UserOpcode.GUEST_MESSAGE, message.getText()));

                    chat.append("[Me]: " + message.getText() + "\n");
                    chat.setCaretPosition(chat.getDocument().getLength());
                    message.setText("");
                }
            }
        });
    }

    public void displayMessage(String message) {
        chat.append(message + "\n");
        chat.setCaretPosition(chat.getDocument().getLength());
    }

    public void disableMessageField() {
        message.setEnabled(false);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
