package com.livechat.testclient;

import com.livechat.shared.Packet;
import com.livechat.testclient.opcodes.ServerOpcode;
import com.livechat.testclient.opcodes.UserOpcode;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection implements Runnable {

    private final TestClient client;

    private Socket socket = null;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    public Connection(TestClient client) {
        this.client = client;

        try {
            socket = new Socket("localhost", 6688);
            out = new ObjectOutputStream(socket.getOutputStream());

            client.getMainForm().displayMessage("-- Connected with server, waiting for support...");
            send(new Packet(UserOpcode.GUEST_MESSAGE, 0, "guest"));
        } catch (UnknownHostException e) {
            System.out.println("Unknown host!");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("I/O error!");
            System.exit(1);
        }
    }

    public void send(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
        } catch (IOException e) {
            System.out.println("Packet sending error!");
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        while(true) {
            try {
                try {
                    Packet packet = (Packet) in.readObject();
                    ServerOpcode.getByID(packet.getOpcodeID()).handle(client, packet);
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Thread sleep error!");
            }
        }
    }
}
