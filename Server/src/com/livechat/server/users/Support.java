package com.livechat.server.users;

import com.livechat.server.Server;

import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Support extends User {
    public Support(Socket socket, ObjectInputStream in, Server server) {
        super(socket, in, server);
        server.sendGuestsCount(this);
    }

    public String toString() {
        return "[Support #" + id + "]";
    }

    public void disconnected() {
        super.disconnected();
        server.removeSupport(this);
    }
}
