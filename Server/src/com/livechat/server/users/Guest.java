package com.livechat.server.users;

import com.livechat.server.Server;

import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Guest extends User {
    public Guest(Socket socket, ObjectInputStream in, Server server) {
        super(socket, in, server);
        server.sendGuestConnected();
    }

    @Override
    public synchronized void addPartner(User partner) {
        // Don't allow multiple partners for guests
        if (partners.isEmpty()) {
            super.addPartner(partner);
        }
    }

    public String toString() {
        return "[Guest #" + id + "]";
    }

    public void disconnected() {
        super.disconnected();
        server.removeGuest(this);
    }
}
