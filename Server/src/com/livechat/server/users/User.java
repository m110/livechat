package com.livechat.server.users;

import com.livechat.server.Server;
import com.livechat.server.core.Connection;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.shared.Packet;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;

public abstract class User {

    private final Socket socket;
    private final Connection connection;
    protected final Server server;

    private ObjectOutputStream out;
    private ObjectInputStream in;

    protected int id;
    protected final LinkedList<User> partners;

    protected Packet packet;

    private static int currentID = 0;

    public User(Socket socket, ObjectInputStream in, Server server) {
        this.socket = socket;
        this.in = in;
        connection = new Connection(this, server);
        this.server = server;

        partners = new LinkedList<User>();

        id = ++currentID;

        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();

            new Thread(connection).start();

            System.out.println("New connection: " + this);
        } catch(IOException e) {
            System.out.println("User: socket error!");
            e.printStackTrace();
        }
    }

    public int getID() {
        return id;
    }

    public void send(Packet packet) {
        try {
            out.writeObject(packet);
            out.flush();
        } catch (IOException e) {
            System.out.println("Sending packet error!");
            e.printStackTrace();
        }
    }

    public void send(ServerOpcode opcode, User sender, String message) {
        send(new Packet(opcode, sender.getID(), message));
    }

    public void send(ServerOpcode opcode, User sender) {
        send(opcode, sender, "");
    }

    public void send(ServerOpcode opcode, String message) {
        send(new Packet(opcode, 0, message));
    }

    public void send(ServerOpcode opcode) {
        send(opcode, "");
    }

    public Packet read() {
        try {
            packet = (Packet) in.readObject();
            return packet;
        } catch (EOFException e) {
           return null;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Packet getPacket() {
        return packet;
    }

    public User getPartnerByID(int id) {
        for (User partner : partners) {
            if (id == partner.getID()) {
                return partner;
            }
        }
        return null;
    }

    public User getFirstPartner() {
        return partners.peekFirst();
    }

    public LinkedList<User> getPartners() {
        return partners;
    }

    public synchronized void addPartner(User partner) {
        partners.offer(partner);
    }

    public synchronized boolean removePartner(User partner) {
        return partners.remove(partner);
    }

    public void disconnected() {
        System.out.println(this + " disconnected.");
    }
}
