package com.livechat.server.core;

import com.livechat.server.Server;
import com.livechat.server.opcodes.UserOpcode;
import com.livechat.server.users.User;
import com.livechat.shared.Packet;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Connection implements Runnable {
    private final User user;
    private final Server server;

    public Connection(User user, Server server) {
        this.user = user;
        this.server = server;
    }

    public void run() {
        while (true) {
            Packet packet = user.read();

            // Can't read the socket - client disconnected
            if (packet == null) {
                user.disconnected();
                return;
            }

            server.printGuestsQueue();

            UserOpcode opcode = UserOpcode.getByID(packet.getOpcodeID());
            opcode.handle(user, server);
        }
    }
}
