package com.livechat.server;

import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.Guest;
import com.livechat.server.users.Support;
import com.livechat.server.users.User;
import com.livechat.shared.Packet;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class Server {
    private static final int PORT = 6688;

    private final ArrayList<Support> supports;
    private final LinkedList<Guest> guests;

    public Server() {
        supports = new ArrayList<>();
        guests = new LinkedList<>();

        try {
            ServerSocket server = new ServerSocket(PORT);
            System.out.println("Server started on port " + PORT + "...");

            while (true) {
                Socket clientSocket = server.accept();

                ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
                try {
                    Packet packet = (Packet) in.readObject();
                    switch (packet.getMessage()) {
                        case "support": addSupport(new Support(clientSocket, in, this)); break;
                        case "guest": addGuest(new Guest(clientSocket, in, this)); break;
                        default:
                            System.out.println("Unknown user type: " + packet.getMessage());
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            System.out.println("main loop: Exception was caught. Exiting.");
            e.printStackTrace();
        }
    }

    public synchronized void addSupport(Support support) {
        supports.add(support);
    }

    public synchronized void addGuest(Guest guest) {
        guests.offer(guest);
    }

    public synchronized void removeSupport(Support support) {
        supports.remove(support);

        for (User partner : support.getPartners()) {
            partner.removePartner(support);
            partner.send(ServerOpcode.CLOSE);
        }
    }

    public synchronized void removeGuest(Guest guest) {
        guests.remove(guest);

        User partner = guest.getFirstPartner();
        if (partner != null) {
            partner.removePartner(guest);
            partner.send(ServerOpcode.CLOSE, guest);
        }

        // Ensure that all supports got proper guests count
        sendGuestsCount();
    }

    public void sendGuestsCount(Support support) {
        support.send(ServerOpcode.GUESTS_AMOUNT, Integer.toString(guests.size()));
    }

    public void sendGuestsCount() {
        for (Support support : supports) {
            sendGuestsCount(support);
        }
    }

    public void sendGuestConnected() {
        for (Support support : supports) {
            support.send(ServerOpcode.NEW_GUEST);
        }
    }

    public synchronized Guest popFirstGuest() {
        return guests.pollFirst();
    }

    public void printGuestsQueue() {
        System.out.print("Guests Queue: ");
        for (Guest guest : guests) {
            System.out.print(guest + " ");
        }
        System.out.println();
    }

    public static void main(String args[]) {
        new Server();
    }
}
