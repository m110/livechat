package com.livechat.server.opcodes;

import com.livechat.server.Server;
import com.livechat.server.opcodes.commands.*;
import com.livechat.server.users.User;
import com.livechat.shared.Opcode;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum UserOpcode implements Opcode {
    GUEST_MESSAGE(1, new GuestMessage()),
    GUEST_CLOSE(2, new GuestClose()),
    SUPPORT_CONNECT(3, new SupportConnect()),
    SUPPORT_MESSAGE(4, new SupportMessage()),
    SUPPORT_CLOSE(5, new SupportClose());

    private final int id;
    private final OpcodeCommand opcodeCommand;

    UserOpcode(int id, OpcodeCommand opcodeCommand) {
        this.id = id;
        this.opcodeCommand = opcodeCommand;
    }

    public void handle(User user, Server server) {
        opcodeCommand.handle(user, server);
    }

    @Override
    public int getID() {
        return id;
    }

    public static UserOpcode getByID(int id) {
        for (UserOpcode opcode : UserOpcode.values()) {
            if (id == opcode.getID()) {
                return opcode;
            }
        }
        throw new IllegalArgumentException("Such UserOpcode ID does not exist: " + id);
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
