package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class SupportMessage implements OpcodeCommand {
    @Override
    public void handle(User user, Server server) {
        int guestID = user.getPacket().getSenderID();
        String message = user.getPacket().getMessage();

        User guest = user.getPartnerByID(guestID);
        if (guest != null) {
            System.out.println("\tGuest: " + guest.getID());
            System.out.println("\tMessage: " + message);
            guest.send(ServerOpcode.MESSAGE, user, message);
        } else {
            System.out.println("SupportMessage: guest not found!");
        }
    }
}
