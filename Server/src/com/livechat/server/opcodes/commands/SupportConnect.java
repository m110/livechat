package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.Guest;
import com.livechat.server.users.Support;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class SupportConnect implements OpcodeCommand {
    @Override
    public void handle(User user, Server server) {
        Guest guest = server.popFirstGuest();

        if (guest != null) {
            System.out.println("\tFirst guest: " + guest);

            user.addPartner(guest);
            guest.addPartner(user);

            server.sendGuestsCount((Support) user);
            user.send(ServerOpcode.CONNECT, guest);
            guest.send(ServerOpcode.CONNECT, user);
        } else {
            System.out.println("SupportConnect: first guest not found!");
        }
    }
}
