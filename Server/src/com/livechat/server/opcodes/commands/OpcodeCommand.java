package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public interface OpcodeCommand {
    public void handle(User user, Server server);
}
