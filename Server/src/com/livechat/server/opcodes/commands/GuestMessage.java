package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class GuestMessage implements OpcodeCommand {
    @Override
    public void handle(User user, Server server) {
        User partner = user.getFirstPartner();
        String message = user.getPacket().getMessage();

        if (partner != null) {
            System.out.println("\tPartner: " + partner);
            System.out.println("\tMessage: " + message);
            partner.send(ServerOpcode.MESSAGE, user, message);
        } else {
            System.out.println("GuestMessage: guest's partner not found!");
        }
    }
}
