package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class SupportClose implements OpcodeCommand {
    @Override
    public void handle(User user, Server server) {
        int guestID = user.getPacket().getSenderID();

        User guest = user.getPartnerByID(guestID);
        if (guest != null) {
            System.out.println("\tGuest: " + guest);

            user.removePartner(guest);
            guest.removePartner(user);

            guest.send(ServerOpcode.CLOSE, user);
        } else {
            System.out.println("SupportClose: guest not found!");
        }
    }
}
