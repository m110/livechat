package com.livechat.server.opcodes.commands;

import com.livechat.server.Server;
import com.livechat.server.opcodes.ServerOpcode;
import com.livechat.server.users.User;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public class GuestClose implements OpcodeCommand {
    @Override
    public void handle(User user, Server server) {
        User partner = user.getFirstPartner();

        if (partner != null) {
            System.out.println("\tPartner: " + partner);
            partner.send(ServerOpcode.CLOSE, user);
        } else {
            System.out.println("GuestClose: guest's partner not found!");
        }
    }
}
