package com.livechat.server.opcodes;

import com.livechat.shared.Opcode;

/**
 * @author m1_10sz <m110@m110.pl>
 */
public enum ServerOpcode implements Opcode {
    CONNECT(1),
    MESSAGE(2),
    CLOSE(3),
    NEW_GUEST(4),
    GUESTS_AMOUNT(5);

    private final int id;

    ServerOpcode(int id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }
}
